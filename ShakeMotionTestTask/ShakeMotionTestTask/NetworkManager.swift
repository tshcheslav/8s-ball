//
//  NetworkManager.swift
//  ShakeMotionTestTask
//
//  Created by 1 on 17.01.2022.
//

import Foundation

class NetworkManager {
    private init() {}
    
    static let shared:NetworkManager = NetworkManager()
    func getAnswear( result: @escaping ((AnswerModel?) -> ())) {
        let url = URL(string: "https://8ball.delegator.com/magic/JSON/Is?")
        let request = URLRequest(url: url!)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if error == nil {
                let decoder = JSONDecoder()
                var decoderAnswerModel:AnswerModel?
                
                if data != nil {
                    decoderAnswerModel = try? decoder.decode(AnswerModel.self, from: data!)
                }
                result(decoderAnswerModel!)
                print(decoderAnswerModel?.magic?.answer!)
            } else {
                print(error as Any)
            }
        } .resume()
    }
}
