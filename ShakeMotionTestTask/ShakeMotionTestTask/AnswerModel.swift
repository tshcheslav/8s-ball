//
//  AnswerModel.swift
//  ShakeMotionTestTask
//
//  Created by 1 on 17.01.2022.
//

import Foundation
// MARK: - Postid
class AnswerModel: Codable {
    let magic: Magic?
}

// MARK: - Magic
class Magic: Codable {
    let answer: String?
}
