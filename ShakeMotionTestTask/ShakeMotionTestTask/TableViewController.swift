//
//  TableViewController.swift
//  ShakeMotionTestTask
//
//  Created by 1 on 14.01.2022.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {
    
    var answers: [Answears] = []
    
    @IBAction func addAnswear(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "NewAnswear", message: "AddIt?", preferredStyle: .alert)
        let saveAnswear = UIAlertAction(title: "add Answear", style: .default) { action in
            let tf = alertController.textFields?.first
            if let newAnswear = tf?.text {
                self.saveAnswear(title: newAnswear)
                self.tableView.reloadData()
            }
        }
        
        alertController.addTextField { _ in}
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(saveAnswear)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // сохранение
    
    func saveAnswear (title: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Answears", in: context) else {return}
        
        let answearObject = Answears(entity: entity, insertInto: context)
        answearObject.answear = title
        do {
            try context.save()
            answers.append(answearObject)
        } catch  let error as NSError {
            print(error.localizedDescription)
        }
                
    }
    
    // получение данных
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<Answears> = Answears.fetchRequest()
        
        do {
            answers = try context.fetch(fetchRequest)
            
        }catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? TableViewCell else {fatalError()}

        let ans = answers[indexPath.row]
        cell.answearLabel.text = ans.answear

        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            context.delete(answers[indexPath.row] as NSManagedObject)
            answers.remove(at:indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.reloadData()

            
            do {
                try context.save()
            } catch  let error as NSError {
                print(error.localizedDescription)
            }
            
            
            
            
        }
        
    }
    

}
