//
//  ViewController.swift
//  ShakeMotionTestTask
//
//  Created by 1 on 14.01.2022.
//

import UIKit
import CoreData
import Reachability


class ViewController: UIViewController {
    
    var answers: [Answears] = []
    
    var prepearedAnswear: String? {
        didSet{
            DispatchQueue.main.async {
                self.answearLabel.text = "\(self.prepearedAnswear!)"
            }
            
        }
    }
    
    @IBOutlet weak var answearLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!
    
    @IBAction func toSettings(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "toSettings", sender: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        becomeFirstResponder()
        self.answearLabel.text = "Whaiting.."
        
    }
    
    override var  canBecomeFirstResponder: Bool {
        return true
    }
    
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            self.actionLabel.isHidden = true
            self.answearLabel.text = "Generating.."
        }
    }
    
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            let reachability = try! Reachability()
            
            if reachability.connection != .unavailable {
                DispatchQueue.main.async {
                    
                NetworkManager.shared.getAnswear(result: { (model) in
                    guard let model = model,let magic = model.magic else {return}
                    self.prepearedAnswear = magic.answer!
                    })
                    self.actionLabel.isHidden = false
                    self.actionLabel.text = "You can try it again."
                }
                
            } else {
                
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let context = appDelegate.persistentContainer.viewContext
                    
                    let fetchRequest: NSFetchRequest<Answears> = Answears.fetchRequest()
                    
                    do {
                        self.answers = try context.fetch(fetchRequest)
                        
                    }catch let error as NSError {
                        print(error.localizedDescription)
                    }
                DispatchQueue.main.async {
                    if self.answers.count > 0 {
                        let list = self.answers.randomElement()
                        
                        self.prepearedAnswear = list?.answear
                        self.actionLabel.isHidden = false
                        self.actionLabel.text = "You can try it again."
                        
                    } else {
                        self.showAlertAction(title: "Your Intrnet conncection is fail.", message: "Chek your internet connection or add some answer in local settings.", url: URL(string: "App-prefs:General&path=ManagedConfigurationLis"))
                       
                        self.actionLabel.isHidden = false
                        self.actionLabel.text = "Chek internet connection or add you own answer in setting."
                        
                    }
                }
            }
        }
        
        
    }
    
    func showAlertAction(title:String, message:String?, url:URL?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let mainSetting = UIAlertAction(title: "Go to Settings", style: .default) { alert in
            if  let url = url {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        let localSettings = UIAlertAction(title: "Answer Settings", style: .default,  handler: {(action:UIAlertAction!)-> Void in
            
            self.performSegue(withIdentifier: "toSettings", sender: self)

        })

        
        
        alert.addAction(mainSetting)
        alert.addAction(localSettings)
        present(alert, animated: true, completion: nil)
        
    }
}
